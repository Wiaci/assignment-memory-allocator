#include <string.h>
#include "tests.h"
#include "stdio.h"
#include "stdlib.h"

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Введите аргумент: номер теста(1-5)\n");
        return 0;
    }

    char* endptr = "";
    long int test_num = strtol(argv[1], &endptr, 10);
    if (!(strcmp(argv[1], "\0") != 0 && strcmp(endptr, "\0") == 0)) {
        printf("Неверный формат ввода аргумента\n");
        return 0;
    }

    switch (test_num) {
        case 1: {
            first_test();
            return 0;
        }
        case 2: {
            second_test();
            return 0;
        }
        case 3: {
            third_test();
            return 0;
        }
        case 4: {
            fourth_test();
            return 0;
        }
        case 5: {
            fifth_test();
            return 0;
        }
        default: return 0;
    }
}