#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t region_actual_sz = region_actual_size(query);
    void* mapped_area_pointer = map_pages(addr, region_actual_sz, MAP_FIXED_NOREPLACE);
    if (mapped_area_pointer == MAP_FAILED) {
        mapped_area_pointer = map_pages(NULL, region_actual_sz, 0);
        if (mapped_area_pointer == MAP_FAILED) {
            return REGION_INVALID;
        }
    }
    block_size sz = {.bytes = region_actual_sz};
    block_init(mapped_area_pointer, sz, NULL);

    struct region region = {
            .addr = mapped_area_pointer,
            .size = region_actual_sz,
            .extends = mapped_area_pointer == addr
    };
    return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

void* debug_heap_init( void const * addr, size_t query ) {
    const struct region region = alloc_region( addr, query );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (!block_splittable(block, query)) return false;
    block_capacity capacity = {.bytes = query};
    if (query < BLOCK_MIN_CAPACITY) {
        capacity.bytes = BLOCK_MIN_CAPACITY;
    }
    block_capacity new_block_capacity = (block_capacity) {
            .bytes = block->capacity.bytes - capacity.bytes - offsetof(struct block_header, contents)
    };
    block_size new_block_size = size_from_capacity(new_block_capacity);

    block->capacity = capacity;
    void* new_block_addr = block->contents + block->capacity.bytes;

    block_init(new_block_addr, new_block_size, block->next);
    block->next = new_block_addr;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block->next == NULL || !mergeable(block, block->next)) return false;
    struct block_header* next = block->next;
    block->capacity.bytes += size_from_capacity(next->capacity).bytes;
    block->next = next->next;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header* b = block;
    struct block_header* prev;
    while (b) {
        if (b->is_free && block_is_big_enough(sz, b)) {
            return (struct block_search_result) {
                .type = BSR_FOUND_GOOD_BLOCK,
                .block = b
            };
        }
        prev = b;
        b = b->next;
    }
    return (struct block_search_result) {
        .type = BSR_REACHED_END_NOT_FOUND,
        .block = prev
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result bsr = find_good_or_last(block, query);
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(bsr.block, query);
    }
    return bsr;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    void* end_addr = last->contents + last->capacity.bytes;
    struct region region = alloc_region(end_addr, query);
    if (region_is_invalid(&region)) return NULL;
    last->next = region.addr;
    if (try_merge_with_next(last)) return last;
    else return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        bsr.block->is_free = false;
        return bsr.block;
    }
    grow_heap(bsr.block, query + offsetof(struct block_header, contents));
    bsr = try_memalloc_existing(query, heap_start);
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        bsr.block->is_free = false;
        return bsr.block;
    }
    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static bool try_merge_with_previous(struct block_header* block, struct block_header* start) {
    if (block == start) return false;
    struct block_header* b = start;
    while (b) {
        if (b->next == block) {
            return try_merge_with_next(b);
        }
        b = b->next;
    }
    return false;
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
    try_merge_with_next(header);
    try_merge_with_previous(header, HEAP_START);
}
