#include "tests.h"
#include "mem.h"

void debug_h() {
    debug_heap(stdout, HEAP_START);
}

void first_test() {
    heap_init(200);
    debug_h();
    void* addr = _malloc(10);
    debug_h();
    _free(addr);
    debug_h();
}

void second_test() {
    heap_init(200);
    debug_h();
    void* addr1 = _malloc(10);
    debug_h();
    void* addr2 = _malloc(50);
    printf("Some action with addr2 to avoid warning: %ld\n", (int64_t) addr2);
    debug_h();
    _free(addr1);
    debug_h();
}

void third_test() {
    heap_init(200);
    debug_h();
    void* addr1 = _malloc(10);
    debug_h();
    void* addr2 = _malloc(50);
    debug_h();
    void* addr3 = _malloc(30);
    debug_h();
    _free(addr3);
    debug_h();
    _free(addr1);
    debug_h();
    _free(addr2);
    debug_h();
}

void fourth_test() {
    heap_init(200);
    debug_h();
    void* addr1 = _malloc(10);
    debug_h();
    void* addr2 = _malloc(50);
    debug_h();
    void* addr3 = _malloc(8700);
    debug_h();
    _free(addr3);
    debug_h();
    _free(addr1);
    debug_h();
    _free(addr2);
    debug_h();
}

void fifth_test() {
    heap_init(200);
    debug_h();
    debug_heap_init(((void*)0x4042000), 200);
    void* addr1 = _malloc(10);
    debug_h();
    void* addr2 = _malloc(50);
    debug_h();
    void* addr3 = _malloc(8700);
    debug_h();
    _free(addr3);
    debug_h();
    _free(addr1);
    debug_h();
    _free(addr2);
    debug_h();
}