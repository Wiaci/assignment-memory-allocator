#ifndef ALLOCATOR_TESTS_H
#define ALLOCATOR_TESTS_H

void start_testing();
void first_test();
void second_test();
void third_test();
void fourth_test();
void fifth_test();

#endif //ALLOCATOR_TESTS_H
